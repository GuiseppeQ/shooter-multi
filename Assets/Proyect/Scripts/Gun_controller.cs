using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_controller : MonoBehaviour
{
    #region Variables
    public PlayerController controller;
    public Gun[] guns;
    public Gun actualGun;
    public int index = 0;
    public int maxGuns = 3;
    public int previous_weapon;

    float lastShootTime = 0;
    float lastReload = 0;
    bool reloading = false;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;

    public GameObject preBulletHole;
    #endregion

    #region Unity Functions
    private void Update()
    {
        if (actualGun != null)
        {


            if (lastShootTime <= 0 && !reloading)
            {

                if (!actualGun.data.automatic)
                {


                    if (Input.GetButtonDown("Fire1"))
                    {

                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }

                    else
                    {
                        if (Input.GetButton("Fire1"))
                        {
                            if (actualGun.data.actualAmmo > 0)
                            {
                                Shoot();
                            }
                        }


                    }
                }
            }

            if (Input.GetButtonDown("Reload") && !reloading)
            {
                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    lastReload = 0;
                    reloading = true;
                }
            }
        }

        else
        {

        }




        if (lastShootTime >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }


        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }
        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        controller.recoil.localRotation = Quaternion.Euler(currentRotation);

        previous_weapon = index;

        if (Input.GetAxis("Mouse ScrollWheel") > 0 && !reloading)
        {
            if (index >= guns.Length - 1)
            {
                index = 0;
            }

            else
            {
                index++;
            }

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0 && !reloading)
        {
            if (index >= guns.Length - 1)
            {
                index = guns.Length - 1;
            }

            else
            {
                index--;
            }

        }

        if (Input.GetButtonDown("One") && !reloading)
        {
            if (index >= guns.Length - 1)
            {
                index = 0;
            }

            else
            {
                index++;
            }
        }

        if (previous_weapon != index)
        {
            Swicth_Weapon();
        }

    }

    #endregion

    #region My Functions
    void Shoot()
    {
        if (Physics.Raycast(controller.cam.transform.position, controller.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
        {
            if (hit.transform != null)
            {
                Debug.Log($"Me Shooting at : { hit.transform.name}");
                Instantiate(preBulletHole, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal, Vector3.up));

            }

        }
        actualGun.data.actualAmmo--;
        lastShootTime = actualGun.data.fireRate;
        Addrecoil();
    }

    void Addrecoil()
    {
        targetRotation += new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
    }

    void Reload()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
    }

    void Swicth_Weapon()
    {
        int i = 0;
        foreach (Gun weapon in guns)
        {
            if (i == index)
            {
                weapon.gameObject.SetActive(true);
            }

            else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }

    }
    #endregion
}
