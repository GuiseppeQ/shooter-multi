using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region variables
    [SerializeField] internal Camera cam;
    public Transform recoil;
    public Transform gunPoint;
    [SerializeField] private Transform point_Of_View;
    [SerializeField] public float walkSpeed;
    [SerializeField] public float runSpeed;
    [SerializeField] private CharacterController controller;
    [SerializeField] private float jumpforce;
    [SerializeField] private float gravityMod = 2.5f;

    private float horizontalRotationStore;
    private float verticalRotationStore;
    public Vector2 mouseInput;
    public Vector3 direction;
    public Vector3 movement;
    private float currentSpeed;

    [Header("Ground Detection")]
    [SerializeField] private bool isGrounded;
    public Vector3 offset;
    public float sphereRadius;
    public float sphereDistance;
    public LayerMask lm;




    #endregion

    #region unity Functions
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, sphereRadius);
        if (isGrounded = (Physics.SphereCast(transform.position + offset, sphereRadius, Vector3.down, out RaycastHit hit, sphereDistance, lm)))
        {
            Gizmos.color = Color.blue;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * sphereDistance);
            Gizmos.DrawWireSphere(endPoint, sphereRadius);
            Gizmos.DrawLine(transform.position + offset, endPoint);
            Gizmos.DrawSphere(hit.point, 0.1f);
        }

        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * sphereDistance);
            Gizmos.DrawWireSphere(endPoint, sphereRadius);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }
    }
    private void Update()
    {
        Rotate_Character();
        Movement();

    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.transform.position;
        cam.transform.rotation = recoil.transform.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }


    #endregion


    #region Functions
    void Rotate_Character()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;
        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60, 60);
        transform.rotation = Quaternion.Euler(0f, horizontalRotationStore, 0f);
        point_Of_View.transform.localRotation = Quaternion.Euler(verticalRotationStore, transform.rotation.y, 0f);
    }

    void Movement()
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        float sg = movement.y;
        movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized;
        movement.y = sg;

        if (Input.GetButton("Fire3"))
        {
            currentSpeed = runSpeed;
        }

        else
        {
            currentSpeed = walkSpeed;
        }

        if (controller.isGrounded)
        {
            movement.y = 0;
        }

        if (Input.GetButtonDown("Jump") && Isground())
        {
            movement.y = jumpforce * Time.deltaTime;
        }
        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement * (currentSpeed * Time.deltaTime));
    }

    private bool Isground()
    {
        isGrounded = false;
        if (isGrounded = (Physics.SphereCast(transform.position + offset, sphereRadius, Vector3.down, out RaycastHit hit, sphereDistance, lm)))
        {

            isGrounded = true;


        }
        return isGrounded;
    }
    #endregion
}
