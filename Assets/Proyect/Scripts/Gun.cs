using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables
    public GunData data;

    #endregion

    #region unity functions
    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }

    #endregion
}
