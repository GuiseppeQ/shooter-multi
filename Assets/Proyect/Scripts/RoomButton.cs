using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    #region Variable
    [SerializeField] TMP_Text buttonText;
    public RoomInfo info;
    #endregion

    #region Custom funcion
   public void SetButtonDetails(RoomInfo infoRoom)
   {
        infoRoom = info;

   }

    public void JoinRoom()
    {
        Launcher.Instance.JoinRoom(info);
    }


    #endregion

}
