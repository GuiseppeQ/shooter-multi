using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;
    [SerializeField] GameObject[] screenObjects;
    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_InputField roomNameInput;
    [SerializeField] TMP_Text texts;
    [SerializeField] TMP_Text error;
    [SerializeField] RoomButton roomButton;
    [SerializeField] Transform contentScroll;
    [SerializeField] List<RoomButton> roomsList = new List<RoomButton>();
    #endregion

    #region Unity Funcions
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        infoText.text = "Connecting to Network.....";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    #endregion

    #region Custom Functions
    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }
    #endregion

    #region Photon

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }

    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(roomNameInput.text);
            infoText.text = "Creating Room...";
            SetScreenObjects(0);
        }
    }

    public override void OnJoinedRoom()
    {
        texts.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(4);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        error.text = "Failed on created a Room " + message;
        SetScreenObjects(2);
    }

    public void ReturnMenu()
    {
        PhotonNetwork.LeaveRoom();
        infoText.text = "Leaving Room";
        SetScreenObjects(0);
    }

    public override void OnLeftRoom()
    {
        SetScreenObjects(1);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {

        bool createButton = false;
        foreach (RoomInfo ri in roomList)
        {
            if (ri.RemovedFromList)
            {
                for (int i = 0; i < roomsList.Count; i++)
                {
                    if (ri.Name == roomsList[i].info.Name)
                    {

                        GameObject go = roomsList[i].gameObject;
                        roomsList.Remove(go.GetComponent<RoomButton>());
                        Destroy(go);
                    }
                }
            }

            for (int i = 0; i < roomsList.Count; i++)
            {
                if (ri.Name == roomsList[i].info.Name)
                {
                    createButton = true;

                }
            }
        }



        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList && !createButton)
            {
                RoomButton newRoomButton = Instantiate(roomButton, contentScroll);
                newRoomButton.SetButtonDetails(roomList[i]);
                roomsList.Add(newRoomButton);
                Debug.Log(roomList);
            }

        }

    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joinig Room...";
        SetScreenObjects(0);
    }

    #endregion
}
